# Final DeCharla

Repositorio de plantilla para el proyecto final del curso 2022-2023.

# ENTREGA CONVOCATORIA XXX

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Mario Cobo Martinez
* Titulación: Ingeniería Telemática
* Cuenta en laboratorios: mcobo
* Cuenta URJC: m.cobom.2019
* Video básico (url):https://www.youtube.com/watch?v=uL3W_doxBV0
* Video parte opcional (url):https://youtu.be/bJ3iZWZdaE0
* Despliegue (url): https://marioocobo.pythonanywhere.com/DeCharla/
* Contraseñas: 12345,  xx34d23
* Contraseña para PythonAnywhere: 12345
* Cuenta Admin Site: admin/admin

## Resumen parte obligatoria
Nada mas entrar en la aplicacion aparece una pagina que te dice que no estas inciado sesion en la aplicacion, todos los botones te llevan al login y si intentas meter un recurso no te lo admite, te lleva de nuevo a login.

Una vez metida la contraseña se inicia sesion y nos aparece un template dandonos la bienvenida, este nos indica que vayamos a la pagina principal a la que podemos acceder desde el logo de la cabecera(DeCharla) como desde el boton de inicio que nos aparece.
Si vamos a la pagina principal vemos que tenemos el logo Decharla con el cual podemos volver a la pagina principal, el boton ayuda donde viene una pequeña descripcion de la aplicacion y tambien tiene los botones para navegar por la aplicacion y sus diferentes recursos, tambien tenemos el boton configuracion donde podemos cambiar el nombre del autor que pone mensajes en las salas, asi como el tipo de letra y el tamaño de esta. Tambien tenemos el boton del admin site, un boton logout con el que podemos crerrar sision y un boton salasJSON que nos devuelve todas las salas y sus mensajes en formato JSON.
En la pagina principal tambien nos aparece con que nombre tenemos para escribir mensajes en las salas (por defecto es Anonymous).
Ademas tenemos un listado con el enlace de cada sala (el enlace es su nombre) y al lado de cada uno de estos enlaces a sala tenemos dos numero, el primero que nos indica el numero total de mensajes en sala y el segundo que nos indica el numero de mensajes que hay en la sala NO Leidos desde la ultima vez que el navegador que esta inciado sesion entró por ultima vez a dicha sala.
Tambien tenemos dos formularios uno para crear una nueva sala(danadola un nombre(clave)) y tambien tengo un buscador de salas donde si le pones la clave de la sala(su nombre) te redirecciona a esta si existe sino, te recarga la pagina y te vuelves a quedar en la pagina principal.
Como pie de pagina principal tenemos la cantidad total de mensajes de texto que hay en la aplicacion así como la cantidad total de imagenes y salas activas.

Si accedemos a una de las salas a traves de su enlace o poniento su recurso, lo que nos encontraremos en a parte de la cabecera con los botones, etc, nos encontraremos con una caja donde podemos enviar mensajes ya sean de texto o imagenes en cuyo caso marcariamos el check para indicar que es una imagen y que por tanto tiene que imprimirse como tal.
Ademas, en las salas tambien tenemos un boton para acceder al recurso JSON de esa sala y un boton para acceder a la sala dinamica.

Si accedemos a la sala dinamica vemos que es lo mismo que la sala normal pero la diferencia esta en que en la sala dinamica se recargan los emnsajes cada pocos segundos automaticamente. En ella tenemos un boton para volver a la sala normal.

Cabe destacar que se pueden enviar mensajes XML a traves de un PUT a las salas pero tiene que llevar la "contraseña" (Authorization) para que se pueda hacer y dentro de Authorization tiene que llevar una de las contraseñas guardada en la aplicacion. He incluido un fichero llamado "FormatoMensajeXML.txt" en el cual incluyo lo que se debe de poner en el body del mensaje PUT del RESTCLIENT. Por si acaso tambien lo adjunto aqui:

        <?xml version="1.0" encoding="UTF-8"?>
        <mensajes>
            <Authorization>Inserte aqui la contraseña</Authorization>
            <mensaje>
                <comentario>Inserte aqui el contenido del Mensaje 1</comentario>
                <isCheck>NoCheck(No es imagen) -- Check(Es imagen)</isCheck>
                <name>Inserte aqui el nombre de la persona que manda el mensaje</name>
            </mensaje>
            <mensaje>
                <comentario>Inserte aqui el contenido del Mensaje 2</comentario>
                <isCheck>NoCheck(No es imagen) -- Check(Es imagen)</isCheck>
                <name>Inserte aqui el nombre de la persona que manda el mensaje</name>
            </mensaje>
        </mensajes>


## Lista partes opcionales

* Nombre parte:favicon
* Nombre parte:logout
* Nombre parte: Buscador De Salas

## Resumen partes opcionales
En cuanto al favicon simplemente lo hemos incorporado con una imagen de un mensaje, y por otro lado, el logout sirve para cerrar sesion y volver a la pagina principal sin estar logeado. Borra las cookies tanto la de username como logged_id como letterType como letterSize.
En cuanto al buscador de salas cabe destacar que se tiene que introducir la clave(nombre) de la sala y si se escribe bien (existe) te redirige a dicha sala, por el contrario, si no existe te redirecciona a la misma pagina en la que estas(pagina principal)

## Despliegue
En cuanto al despliegue lo he conseguido hacer pero hay algunas cosas del static concretamente del css que no me carga y eso que he incluido mi directorio static en el Static Files de pythonAnywhere de todas formas es una cosa menor ya que me lo reconoce casi todo.

