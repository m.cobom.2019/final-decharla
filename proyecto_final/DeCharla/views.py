from django.shortcuts import render, reverse
from django.http import HttpResponse, Http404, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Sala, Mensaje, Password, accesoSala
from django.template import loader #permite manejar el template
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.utils import timezone
from django.db.models import Count
import io
from datetime import datetime
from xml.etree.ElementTree import XMLParser
from xml.etree import ElementTree
from django.http import JsonResponse


import xml.etree.ElementTree as ET


# Funcion conta()
# En esta funcon contamos el numero de mensajes que hay de tipo texto y de tipo imagen
def conta():
    mensajes = Mensaje.objects.all()
    img = 0
    msg = 0
    for m in mensajes:
        if m.isCheck == "Check":
            img = img + 1
        else:
            msg = msg + 1
    return img, msg

# Funcion sactivas()
# En esta funcion contamos la cantidad de salas activas que hay en este momento en la aplicacion
def sactivas():
    ss = Sala.objects.all()
    num=0
    for n in ss:
        num = num + 1
    return num

# Funcion index(request)
# Pagina principal donde se ponen los enlaces a las diferentes salas asi como su numero de mensajes
# total y numero de mensajes no leidos, podemos crear salas nuevas y podemos buscar las salas en un buscador
# (dicho buscador busca el nombre de la sala en el caso de esta practica busca por la clave de las salas que es su
# nombre de referencia)
@csrf_exempt
def index(request):

    if request.method in ["PUT","POST"]:
        if request.method == "POST":
            action = request.POST["action"]
            if action == "Enviar Sala":
                recurso = request.POST["nameSala"]
                if Sala.DoesNotExist:
                    new_content(recurso)
            elif action == "Buscar Sala":
                recursoo = request.POST["namSala"]
                recur = Sala.objects.filter(clave=recursoo).first()
                if recur:
                    return redirect("/DeCharla/" + recursoo)
                else:
                    return redirect("/DeCharla")

    ##content_list = Sala.objects.all()
    imgg, msgg = conta()
    salas = sactivas()
    template = loader.get_template('cms/index.html')
    content_list = Sala.objects.annotate(num_mensajes=Count('mensaje'))
    uuser = request.COOKIES.get('csrftoken')
    saalas = Sala.objects.all()
    accesos_usuario = accesoSala.objects.filter(nombre=uuser)
    ultimosMensajes = {}
    ultimos_accesos = {}

    for acceso in accesos_usuario:
        ultimos_accesos[acceso.sala] = acceso.ultimaFecha

    for s in saalas:
        fecha_ultimo_acceso = ultimos_accesos.get(s, datetime.min)
        num_mensajes = Mensaje.objects.filter(contenido=s, fecha__gt=fecha_ultimo_acceso).count()
        ultimosMensajes[s] = num_mensajes

    """ultimosMensajes = {}

        for s in saalas:
            ultimoAcceso = accesoSala.objects.filter(sala=s, nombre=uuser).first()
            mensajesss = Mensaje.objects.all()
            num = 0
            for m in mensajesss:
                if m.contenido == s:
                    if ultimoAcceso is not None and m.fecha > ultimoAcceso.ultimaFecha:
                        num = num + 1
            ultimosMensajes[s] = num"""
    # else:
        #    ultimosMensajes[s] = 0
        #ultimoAcceso.ultimaFecha = timezone.now()
        #ultimoAcceso.save()
        #else:
            #numeroMensajesN = Mensaje.objects.filter(contenido=s).count()
            #num = 0

    if 'login_status' in request.COOKIES and 'username' in request.COOKIES:
        contexto = {
            'contenido_lista': content_list,
            'username': request.COOKIES['username'],
            'login_status': request.COOKIES.get('login_status'),
            'name': request.COOKIES.get('name'),
            'msg': msgg,
            'img': imgg,
            'salas': salas,
            'ultimosMensajes': ultimosMensajes,
            #'nnn': num_mensajes_sala,

        }
        # 4 Renderizar el template y responder
        return HttpResponse(template.render(contexto, request))
    else:
        context = {}
        return HttpResponse(template.render(context, request))

"""#Función para obtener una lista con todas las salas
def get_sala_lista(request):
    lista_salas = Sala.objects.all() #Obtenemos todods los objetos de tipo sala
    for sala in lista_salas:         #Recorremos la lista de salas
        total_messages = Mensaje.objects.filter(Sala=sala).count() #Obtenemos el total de mensajes de la sala
        last_visit = request.session.get(f"last_visit_{sala.Nombre}") #Obtenemos el número de mensajes desde la ultima visita
        if last_visit:
            if isinstance(last_visit, str):
                last_visit = datetime.datetime.strptime(last_visit, "%Y-%m-%d %H:%M:%S")  # Convertir a datetime
            last_visit_messages = Mensaje.objects.filter(Sala=sala, Fecha__gt=last_visit).exclude(Autor__Sesion=request.COOKIES.get("Sesion")).count()
        else:
            last_visit_messages = total_messages
        sala.total_messages = total_messages #Incluimos dentro de la sala el número total de mensajes
        sala.last_visit_messages = last_visit_messages #Incluimos dentro de la sala el número total de mensajes desde la ultima visita
        request.session[f"last_visit_{sala.Nombre}"] = timezone.now().strftime("%Y-%m-%d %H:%M:%S")
    return lista_salas"""

# Funcion update_content(contenido)
# Esta funcion guarda en la base el contenido que se le pase
def update_content(contenido):
    contenido.save()

# Funcion new_content(recurso)
# guarda la sala cuya clave se le pasa a esta funcion como "recurso"
def new_content(recurso):
    c = Sala(clave=recurso)
    c.save()

# Funcion get_resources(request, recurso)
# A esta funcion se le pasa "recurso" el cual corresponde a la clave de una sala y a partir de hay tiene
# varias funcionalidades: cuando le entra un put lo envia a mensajesXML donde se procesara el mensaje xml
# mandado en forma de PUT. Tambien si le llega un POST con un nuevo recurso (una nueva sala) la crea.
# Tambien registra los mensajes mandados en las salas, nos deriva al formato json de la sala
# nos deriva tambien a la sala dinamica de la propia sala todoo esto comprobando las cookies
# es decir que el usuario tiene que haber escrito correctamente una de las contraseñas.
# En resumen, esta funcion nos muestra toodo lo que hay dentro de una sala y toodo lo que se puede
# hacer dentro de ella.
@csrf_exempt
def get_resources(request, recurso):
    #PUT
    if request.method in ["PUT","POST"]:
        if request.method == "PUT":
            return mensajesXML(request, recurso)
            #Valor = request.body.decode()
            #try:
            #    contenido = Sala.objects.get(clave=recurso)
            #    update_content(contenido)
            #except Sala.DoesNotExist:
            #    new_content(recurso)

        if request.method == "POST":
            action = request.POST["action"]
            if action == "Enviar Contenido":
                #Valor = request.POST["valor"]
                try:
                    contenido = Sala.objects.get(clave=recurso)
                    update_content(contenido)
                except Sala.DoesNotExist:
                    new_content(recurso)
            elif action == "Enviar Mensaje":
                #extraidos del formulario
                mensaje = request.POST["comentario"]
                #contenido tengo que hacer una consulta
                contenido = Sala.objects.get(clave=recurso)
                #nSala = contenido.id
                fecha = timezone.now()
                isC = request.POST.get("check", "NoCheck")
                #confi = Configuracion.objects.latest(id)

                name = request.COOKIES.get('name')
                if name == None:
                    name = "Anonymous"

                c = Mensaje(contenido=contenido, comentario=mensaje, isCheck=isC, fecha=fecha, name=name)
                c.save()
            elif action == "json":
                listt = Mensaje.objects.all()
                salaa = Sala.objects.get(clave=recurso)
                context = {
                    'list': listt,
                    'salaa': salaa,
                }
                return render(request, 'cms/sala.json', context)
            elif action == "salaDin":
                sal = Sala.objects.get(clave=recurso)
                lis = Mensaje.objects.all()
                context = {
                    'lis': lis,
                    'sal': sal,
                }
                return render(request, 'cms/salaDin.html', context)
            elif action == "Enviar":
                mensaje = request.POST["comentario"]
                contenido = Sala.objects.get(clave=recurso)
                fecha = timezone.now()
                isC = request.POST.get("check", "NoCheck")

                name = request.COOKIES.get('name')
                if name == None:
                    name = "Anonymous"

                c = Mensaje(contenido=contenido, comentario=mensaje, isCheck=isC, fecha=fecha, name=name)
                c.save()
                sal = Sala.objects.get(clave=recurso)
                lis = Mensaje.objects.all()
                context = {
                    'lis': lis,
                    'sal': sal,
                }
                return render(request, 'cms/salaDin.html', context)


    #Metodos GET
    if 'login_status' in request.COOKIES and 'username' in request.COOKIES:
        try:
            contenido = Sala.objects.get(clave=recurso)
            coment = Mensaje.objects.all()
            #configg = Configuracion.objects.all()
            coments = list(reversed(coment))
            namm = request.COOKIES.get('csrftoken')
            acceso = accesoSala(sala=contenido, nombre=namm, ultimaFecha=datetime.now())
            acceso.save()

        except Sala.DoesNotExist:
            return render(request, 'cms/alta.html', {})

        num = 0
        for c in coments:
            if c.contenido == contenido:
                num = num + 1
        """
        Esto es el intento de hacer el contador de mensajes desde la ultima conexion
        num2 = 0
        if 'name' in request.COOKIES:
            i = request.COOKIES.get('name')
        else:
            i = "Anonymous"
    
        f = timezone.now()
        g = timezone.now()
        for c in coments:
            if c.contenido == contenido:
                if c.name == i:
                    if f == g:
                        f = c.fecha
    
        for c in coments:
            if c.contenido == contenido:
                if c.name != i:
                    if c.fecha > f:
                        num2 = num2 + 1
    """

        contexto = {'contenido':contenido,
                    'content': coments,
                    'numero': num,
                    'username': request.COOKIES['username'],
                    'login_status': request.COOKIES.get('login_status'),
                    'letterType': request.COOKIES.get('tipo-letra'),
                    'letterSize': request.COOKIES.get('tamano-letra'),
                    #'configg': configg,
                    }
        #if request.path.endswith("din"):
        #    return render(request, 'cms/salaDin.html', contexto)
        #else:
        return render(request,'cms/contenido.html', contexto)

    else:
        return redirect("/DeCharla/login")
        #return render(request, 'registration/login.html', {})


#def loggedIn(request):
 #   if request.user.is_authenticated:
 #       respuesta = "El usuario "+ request.user.username+ " esta autenticado"
  #  else:
   #     respuesta = "No estas logeado, entra en el <a href=/login" \
    #                ">login</a>"
    #return HttpResponse(respuesta)

#def logout_vista(request):
 #   logout(request)
  #  return redirect("/DeCharla/")

# Funcion login(request)
# En esta funcion sirve para visualizar la pagina de login y para comprobar que
# efectivamente un usuario se loggea en la aplicacion y por tanto crea unas cookies que utilizaremos.
@csrf_exempt
def login(request):
    if request.method == 'GET':
        return render(request, 'registration/login.html')
    if request.method == 'POST':
        username = request.POST["password"]
        passw = Password.objects.all()
        for c in passw:
            if username == c.clave:
                contexto = {
                    'username':username,
                    'login_status': True,
                    'name': request.COOKIES.get('name'),
                }
                response = render(request, 'cms/logged_in.html', contexto)
                response.set_cookie('username', username)
                response.set_cookie('login_status', True)
                return response
            else:
                return redirect('DeCharla/')


# Funcion config(request)
# En esta funcion creamos la pagina de configuracion donde podremos cambiar el nombre
# que llevaran asociados los mensajes asi como el tamaño y tipo de letra que tendran dichios mensajes
# dentro de las salas. Toodo ello tambien lo hago con cookies guardando tanto el nombre
# como el tipo y tamaño de la letra
@csrf_exempt
def config(request):
    if request.method == 'GET':
        contexto = {
            'name': request.COOKIES.get('name'),
            'letterType': request.COOKIES.get('tipo-letra'),
            'letterSize': request.COOKIES.get('tamano-letra'),
            'username': request.COOKIES['username'],
            'login_status': request.COOKIES.get('login_status'),
        }
        return render(request, 'cms/configuracion.html', contexto)
    if request.method == 'POST':
        action = request.POST["action"]
        if action == "Cambiar Nombre":
            name = request.POST["name"]
            #letterType = request.POST["tipo-letra"]
            #letterSize = request.POST["tamano-letra"]
            contexto = {
                'name': name,
                #'letterType': letterType,
                #'letterSize': letterSize,
                'username': request.COOKIES['username'],
                'login_status': request.COOKIES.get('login_status'),

            }
            response = render(request, 'cms/configuracion.html', contexto)
            response.set_cookie('name', name)

        if action == "Cambiar Configuracion":
            letterType = request.POST["tipo-letra"]
            letterSize = request.POST["tamano-letra"]
            contexto = {
                #'name': ,
                 'letterType': letterType,
                 'letterSize': letterSize,
                'username': request.COOKIES['username'],
                'login_status': request.COOKIES.get('login_status'),

            }
            response = render(request, 'cms/configuracion.html', contexto)
            response.set_cookie('tipo-letra', letterType)
            response.set_cookie('tamano-letra', letterSize)

        #response = render(request, 'cms/configuracion.html', contexto)
        #response.set_cookie('name', name)
        #response.set_cookie('tipo-letra', letterType)
        #response.set_cookie('tamano-letra', letterSize)
        return response

    #return render(request, 'registration/login.html')

# Funcion logout(request)
# En esta funcion se puede hacer logout de la aplicacion y como consecuencia se borran las cookies creadas.
def logout(request):
    response = HttpResponseRedirect(reverse('home'))
    response.delete_cookie('username')
    response.delete_cookie('login_status')
    response.delete_cookie('name')
    response.delete_cookie('tipo-letra')
    response.delete_cookie('tamano-letra')
    return response


# Funcion schema(request)
# Funcion que sirve para amndar a un template toda la cabecera que utilizaremos en la aplicacion
# para que asi los demas templates del proyecto puedan heredar de este y asi implementar los diferentes
# blocks creados
def schema(request):
    template = loader.get_template('cms/schema.html')
    """mensajes1 = Mensaje.objects.all()
    img = 0
    txt = 0
    for m in mensajes1:
        if m.ischeck == "Check":
            img = img + 1
        else:
            txt = txt + 1
    sss = sactivas()"""
    contexto = {
        'username': request.COOKIES['username'],
        'login_status': request.COOKIES.get('login_status'),
        #'numSalas': sss,
        #'numTextos': txt,
        #'numImagenes': img,
    }

    # 4 Renderizar el template y responder
    return HttpResponse(template.render(contexto, request))


"""
@csrf_exempt
def pag_config(request):
    if request.method == "POST":
        action = request.POST["action"]
        if action == "Enviar Configuracion":
            name = request.POST["namee"]
            tipo = request.POST["ltipo"]
            tamano = request.POST["ltam"]
            con = Configuracion(usuario=name, tipoLetra=tipo, tamLetra=tamano)
            con.save()

    person = Configuracion.objects.all()
    template = loader.get_template("cms/configuracion.html")
    contexto = {
                'person': person,
    }
    return HttpResponse(template.render(contexto, request))
"""
# Funcion pag_ayuda(request)
# Esta funcion nos crea la pagina de ayuda
def pag_ayuda(request):
    template = loader.get_template("cms/ayuda.html")

    contexto = {
        'username': request.COOKIES['username'],
        'login_status': request.COOKIES.get('login_status'),
    }
    return HttpResponse(template.render(contexto, request))


"""def mensajes_json(request):
    list = Mensaje.objects.all()
    template = loader.get_template("cms/mensajes.json")
    context = {
        'lista': list,
    }
    return HttpResponse(template.render(context, request))"""
# Funcion salas_json(request)
# Esta funcion nos saca las diferentes salas creadas en la aplicacion en formato json
def salas_json(request):
    list = Mensaje.objects.all()
    salas = Sala.objects.all()
    template = loader.get_template("cms/salas.json")
    context = {
        'lista': list,
        'salas': salas,
    }
    return HttpResponse(template.render(context, request))
"""def mensajes_xml(request):
    list = Mensaje.objects.all()
    template = loader.get_template("cms/mensajes.xml")
    context = {
        'lista': list,
    }
    return HttpResponse(template.render(context, request))"""

"""def salas_xml(request):
    list = Mensaje.objects.all()
    salas = Sala.objects.all()
    template = loader.get_template("cms/salas.xml")
    context = {
        'lista': list,
        'salas': salas,
    }
    return HttpResponse(template.render(context, request))"""
# Funcion mensajesXML(request,recurso)
# En esta funcion procesamos el contenido de los mensajes que enviamos en formato XML
# a traves de un PUT a la aplicacion (a una sala en concreto)
def mensajesXML(request, recurso):
    try:
        salaa = Sala.objects.get(clave=recurso)
    except Sala.DoesNotExist:
        raise Http404

    datos_xml = request.body
    root = ElementTree.fromstring(datos_xml)
    contraseña = root.find('Authorization').text
    contraseña_valida = Password.objects.filter(clave=contraseña).exists()

    if contraseña_valida:
        for mensaje_xml in root.findall('mensaje'):
            comentario = mensaje_xml.find('comentario').text
            isC = mensaje_xml.find('isCheck').text
            name = mensaje_xml.find('name').text

            nuevoMensaje = Mensaje(contenido=salaa, comentario=comentario, isCheck=isC, fecha=datetime.now(), name=name)
            nuevoMensaje.save()

        response = "Se han guardado los mensajes correctamente"
        return HttpResponse(response, content_type='text/plain', status=201)
    else:
        response = "Contraseña inválida"
        return HttpResponse(response, content_type='text/plain', status=401)


# Funcion mensajes_json(request, sala)
# En esta funcion se saca en formato json todos los mensajes que hay una sala
# podremos acceder a ello a traves de un boton que hay dentro de la sala
def mensajes_json(request, sala):
    mensajes = Mensaje.objects.filter(contenido_id=sala)
    mensajes_json = []
    for mensaje in mensajes:
        mensaje_json = {
            'name': mensaje.name,
            'contenido': mensaje.comentario,
            'isCheck': mensaje.isCheck,
            'fecha': mensaje.fecha,
            
        }
        mensajes_json.append(mensaje_json)

   
    return JsonResponse(mensajes_json, safe=False)

