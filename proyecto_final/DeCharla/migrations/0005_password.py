# Generated by Django 4.1.7 on 2023-05-15 11:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("DeCharla", "0004_alter_mensaje_ischeck"),
    ]

    operations = [
        migrations.CreateModel(
            name="Password",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("clave", models.TextField()),
            ],
        ),
    ]
