# Generated by Django 4.1.7 on 2023-06-14 15:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("DeCharla", "0006_remove_sala_valor"),
    ]

    operations = [
        migrations.CreateModel(
            name="accesoSala",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("nombre", models.TextField()),
                ("ultimaFecha", models.DateTimeField()),
                (
                    "sala",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="DeCharla.sala"
                    ),
                ),
            ],
        ),
    ]
