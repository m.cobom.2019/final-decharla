from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="home"),
    #path('acceso', views.loggedIn),
    #path('logout', views.logout_vista),
    #path('signup', views.signup),
    path('ayuda', views.pag_ayuda),
    path('config', views.config),
    path('login', views.login, name="login"),
    path('logout', views.logout),
    path('<str:recurso>/', views.get_resources),
    path('<str:recurso>', views.get_resources),
    #path('<str:recurso>/din', views.sala_din),
    #path('<str:recurso>/din', views.get_resources),
    #path('salaJSON', views.sala_json, name='jsalaJSON'),
    path('mensajes_json/<int:sala>/', views.mensajes_json, name='mensajes_json'),
    #path('mensajes/format=json', views.mensajes_json),
    path('salas/format=json', views.salas_json),
    #path('mensajes/format=xml', views.mensajes_xml),
    #path('salas/format=xml', views.salas_xml),

]