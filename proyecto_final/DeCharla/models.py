from django.db import models
# Create your models here.
class Sala(models.Model):
    clave = models.CharField(max_length=64)  # Recurso #Como maximo va a tener 64 caracteres
    #valor = models.TextField()  # contenido de la pagina html
    #numMensajes = models.TextField(default=0)

#class Configuracion(models.Model):
 #   usuario = models.TextField()
  #  tipoLetra = models.TextField()
   # tamLetra = models.TextField()
    #tipo_msg = models.TextField()

class Password(models.Model):
    clave = models.TextField()

class Mensaje(models.Model):
    contenido = models.ForeignKey(Sala, on_delete=models.CASCADE)
    comentario = models.TextField()
    isCheck = models.TextField(default="NoCheck")
    fecha = models.DateTimeField()
    name = models.TextField(default="Anonymous")
    #config = models.ForeignKey(Configuracion, on_delete=models.CASCADE, default="default")
    #numSala = models.TextField()

class accesoSala(models.Model):
    sala = models.ForeignKey(Sala, on_delete=models.CASCADE)
    nombre = models.TextField()
    ultimaFecha = models.DateTimeField()